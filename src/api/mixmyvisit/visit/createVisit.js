const axios = require('axios');
const sendResponseViaMessengerAPI = require('../../messenger/sendResponseViaMessengerAPI');

module.exports = async function requestCreateVisit(tagID, sender_psid) {
  try {
    await axios.post(
      'https://mixmyvisit.web.ua.pt/api/webhook/create-visit',
      { psid: sender_psid, tag: tagID },
      { headers: { Authorization: `Bearer ${process.env.API_CLIENT}` } }
    );

    return sendResponseViaMessengerAPI(sender_psid, {
      text:
        'A sua visita foi criada com sucesso, é agora sua visita ativa.\nPode a partir de agora adicionar novas localizações a partir da App à visita, e pode, também, adicionar conteúdos (imagens e vídeos) à sua visita da sua autoria ao recorrer ao comando "adicionar conteudo".\nSe ainda pretender visualizar os detalhes sobre a sua visita pode utilizar o comando "info visita".\nQuando quiser terminar a sua visita pode fazê-lo pelo comando "concluir visita" ou pela aplicação nas estações no botão - "Terminar visita".\nSe necessitar de ajuda utilize o comando "ajuda" ou "comandos"',
    });
  } catch (err) {
    const genericError = {
      text:
        'Pedimos desculpa, ocorreu um erro durante a criação da sua visita. Tente mais tarde ou se o problema continuar a persistir contacte-nos via email: mixmv.ua@gmail.com',
    };

    console.log('--CREATEVISIT-- !!!!!! Error creating visit =>', err);

    if (err.response.data) {
      if (err.response.data.message === 'User has an active visit') {
        return sendResponseViaMessengerAPI(sender_psid, {
          text:
            'Neste momento tem uma visita em decurso o que não permite a realização deste comando. Para visualizar informações sobre a visita insira o comando !info_visita',
        });
      } else {
        return sendResponseViaMessengerAPI(sender_psid, {
          text: genericError,
        });
      }
    } else if (err.request) {
      return sendResponseViaMessengerAPI(sender_psid, {
        text: genericError,
      });
    } else {
      return sendResponseViaMessengerAPI(sender_psid, {
        text: genericError,
      });
    }
  }
};
