const axios = require('axios');
const sendResponseViaMessengerAPI = require('../../messenger/sendResponseViaMessengerAPI');

module.exports = async function checkActiveVisitAndRendering(
  sender_psid,
  activeVisitFunction = null,
  noVisitFunction = null
) {
  try {
    const getActiveVisit = await axios.post(
      'https://mixmyvisit.web.ua.pt/api/webhook/view-active-visit',
      { psid: sender_psid },
      {
        headers: {
          Authorization: `Bearer ${process.env.API_CLIENT}`,
          'Content-Type': 'application/json',
        },
      }
    );

    const activeVisit = getActiveVisit.data.data;
    const visitState = activeVisit.states_id;

    if (visitState === 8 || visitState === 14 || visitState === 2) {
      sendResponseViaMessengerAPI(sender_psid, {
        text:
          'Neste momento possui uma visita ativa que se encontra no processo de renderização tornando o efeito deste comando nulo.',
      });
    } else if (activeVisitFunction) {
      activeVisitFunction(activeVisit.tagID);
    } else {
      sendResponseViaMessengerAPI(sender_psid, {
        text:
          'Neste momento possuí uma visita ativa o que torna o efeito desta ação nulo visto que esta ação é só possível realizar quando não tem uma visita activa em decurso.',
      });
    }
  } catch (err) {
    const genericError = {
      text:
        'Pedimos desculpa mas ocorreu um erro ao verificar se possui uma visita ativa, tente novamente mais tarde.\nSe o problema persistir contacte-nos via email',
    };

    if (err.response.data) {
      if (err.response.data.message === 'User has no active visit') {
        if (noVisitFunction) {
          noVisitFunction();
        } else {
          sendResponseViaMessengerAPI(sender_psid, {
            text:
              'Neste momento não possuí nenhuma visita ativa o que torna o efeito deste comando nulo. Para começar uma nova visita necessita de utilizar recorrer à aplicação MixMyVisit e criar uma visita personalizada e obter um PIN específico para começar a interagir com o bot e utilizar este comando/funcionalidade.',
          });
        }
      } else {
        sendResponseViaMessengerAPI(sender_psid, genericError);
      }
    } else if (err.request) {
      sendResponseViaMessengerAPI(sender_psid, genericError);
    } else {
      sendResponseViaMessengerAPI(sender_psid, genericError);
    }
  }
};
