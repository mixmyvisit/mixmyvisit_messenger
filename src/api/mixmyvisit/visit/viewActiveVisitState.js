const axios = require('axios');
const moment = require('moment');

moment.locale('pt');

module.exports = async function requestViewActiveVisitState(psid) {
  try {
    const getActiveVisit = await axios.post(
      'https://mixmyvisit.web.ua.pt/api/webhook/view-active-visit-state',
      { psid: psid },
      {
        headers: {
          Authorization: `Bearer ${process.env.API_CLIENT}`,
          'Content-Type': 'application/json',
        },
      }
    );
    console.log(
      '--VIEWACTIVEVISITSTATE-- $$$$$$ Got active visit state with success'
    );
    const visitName = getActiveVisit.data.name;
    const visitState = getActiveVisit.data.state;

    const states = {
      1: 'Work in progress',
      2: 'Pronta para ser renderizada',
      3: 'Renderizada',
      4: 'À espera de aprovação',
      5: 'Aprovada',
      6: 'Rejeitada',
      7: 'Publicada na plataforma',
      8: 'A renderizar',
      9: 'A ser processada no Youtube',
      10: 'Erro a renderizar',
      11: 'Erro no Youtobe',
      12: 'Erro ao fazer o upload',
      13: 'Processada no Youtube',
      14: 'Enviada para o renderizador',
      15: 'Para ser removida',
      16: 'Submetida',
      17: 'A fazer download das assets',
    };

    return {
      text: `Atualmente o estado da sua visita ativa, ${visitName}, é:\n${states[visitState]}`,
    };
  } catch (err) {
    const genericError = {
      text:
        'Pedimos desculpa, ocorreu um erro durante o pedido do estado da sua visita ativa. Tente mais tarde ou se o problema continuar a persistir contacte-nos via email: mixmv.ua@gmail.com',
    };

    console.log(
      '--VIEWACTIVEVISITSTATE-- !!!!!! Error getting active visit state =>',
      err.response.data
    );

    if (err.response.data) {
      if (err.response.data.message === 'User has no active visit') {
        return {
          text:
            'Neste momento não possuí nenhuma visita ativa o que torna o efeito deste comando nulo.',
        };
      } else {
        return genericError;
      }
    } else if (err.request) {
      return genericError;
    } else {
      return genericError;
    }
  }
};
