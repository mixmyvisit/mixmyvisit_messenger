const axios = require('axios');

module.exports = async function requestRender(sender_psid, duration) {
  try {
    const renderVisit = await axios.post(
      'https://mixmyvisit.web.ua.pt/api/webhook/render',
      { psid: sender_psid, videosDuration: duration },
      { headers: { Authorization: `Bearer ${process.env.API_CLIENT}` } }
    );
    console.log('--ASKFORRENDER-- $$$$$$ Sent to render with success');
    return {
      text:
        `O vídeo da sua visita está agora a ser renderizado. Este processo de "renderização" poderá demorar alguns minutos.\nQuando o processo terminar receberá uma mensagem neste chat e um email.\nPode também ir verificando em tempo real o processo de render da visita no seguinte link do nosso website: https://mixmyvisit.web.ua.pt/redirect/visita/personalizada/${renderVisit.data.tag}?to=visit`,
    };
  } catch (err) {
    console.log(
      '--ASKFORRENDER-- $$$$$$ Couldnt send to render',
      err.response.data
    );

    const genericError = {
      text:
        'Pedimos desculpa, ocorreu um erro no pedido para iniciar a renderização do vídeo da sua visita. Tente mais tarde ou se o problema continuar a persistir contacte-nos via email: mixmv.ua@gmail.com.',
    };

    if (err.response.data) {
      if (err.response.data.message === 'User has no active visit') {
        return {
          text:
            'Neste momento não possuí nenhuma visita ativa o que torna o efeito deste comando nulo. Para começar uma nova visita necessita de utilizar recorrer à aplicação MixMyVisit e criar uma visita personalizada e obter um PIN específico para começar a interagir com o bot e utilizar este comando/funcionalidade.',
        };
      } else {
        return genericError;
      }
    } else if (err.request) {
      return genericError;
    } else {
      return genericError;
    }
  }
};
