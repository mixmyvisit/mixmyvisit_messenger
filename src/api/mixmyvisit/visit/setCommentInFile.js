const axios = require('axios');

module.exports = async function requestSetComment(sender_psid, file, message) {
  try {
    await axios.post(
      'https://mixmyvisit.web.ua.pt/api/webhook/upload/comment',
      { psid: sender_psid, file: file, comment: message },
      { headers: { Authorization: `Bearer ${process.env.API_CLIENT}` } }
    );

    console.log('--SETCOMMENT-- $$$$$$ Set comment to file');

    return {
      text:
        'O seu comentário foi adicionado ao seu item da visita com sucesso!\nCaso pretenda adicionar outra imagem necessitará de repetir a ação que acabou de realizar. Se desejar complementar a visita com um ficheiro de vídeo utilize o comando "adicionar conteudo" para saber como fazê-lo',
    };
  } catch (err) {
    console.log('--SETCOMMENT-- $$$$$$ Couldnt set comment', err.response.data);

    const genericError = {
      text:
        'Pedimos desculpa, ocorreu um erro durante a adição do comentário ao item da sua visita. Tente mais tarde quando adicionar outra imagem e se o problema continuar a persistir contacte-nos via email: mixmv.ua@gmail.com',
    };

    if (err.response.data) {
      if (err.response.data.message === 'Over 75 characters') {
        return {
          text:
            'Pedimos desculpa, ocorreu um erro durante a adição do comentário ao item da sua visita pois este possuía mais de 100 caracteres.',
        };
      } else {
        return genericError;
      }
    } else if (err.request) {
      return genericError;
    } else {
      return genericError;
    }
  }
};
