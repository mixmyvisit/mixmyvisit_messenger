const axios = require('axios');
const moment = require('moment');

moment.locale('pt');

module.exports = async function requestViewCurrentVisitInfo(psid) {
  try {
    const getActiveVisit = await axios.post(
      'https://mixmyvisit.web.ua.pt/api/webhook/view-active-visit',
      { psid: psid },
      {
        headers: {
          Authorization: `Bearer ${process.env.API_CLIENT}`,
          'Content-Type': 'application/json',
        },
      }
    );

    const activeVisit = getActiveVisit.data.data;
    const activeVisitVideoInfo = JSON.parse(activeVisit.editor_form);

    const states = {
      1: 'Work in progress',
      2: 'Pronta para ser renderizada',
      3: 'Renderizada',
      4: 'À espera de aprovação',
      5: 'Aprovada',
      6: 'Rejeitada',
      7: 'Publicada na plataforma',
      8: 'A renderizar',
      9: 'A ser processada no Youtube',
      10: 'Erro a renderizar',
      11: 'Erro no Youtobe',
      12: 'Erro ao fazer o upload',
      13: 'Processada no Youtube',
      14: 'Enviada para o renderizador',
      15: 'Para ser removida',
      16: 'Submetida',
      17: 'A fazer download das assets',
    };

    const getTotalDuration = (arr) => {
      let dur = 0;
      if (arr) {
        for (let i = 0; i < arr.length; i++) {
          const clipDuration = arr[i].trimEnd - arr[i].trimStart;
          dur += clipDuration;
        }
      }

      const hrs = ~~(dur / 3600);
      const mins = ~~((dur % 3600) / 60);
      const secs = ~~dur % 60;
      let ret = '';

      if (hrs > 0) {
        ret += '' + hrs + ':' + (mins < 10 ? '0' : '');
      }
      ret += '' + mins + ':' + (secs < 10 ? '0' : '');
      ret += '' + secs;
      return ret;
    };

    return {
      text: `Esta é a informação da sua visita ativa:\nNome - ${
        activeVisit.name
      }\nEstado - ${states[activeVisit.states_id]}\nData de criação - ${moment(
        activeVisit.created_at
      ).format('D MMMM YYYY')}\nData da última atualização - ${moment(
        activeVisit.updated_at
      ).format('D MMMM YYYY')}\nNúmero de imagens/vídeos - ${
        activeVisitVideoInfo.video.length
      }\nDuração - ${getTotalDuration(
        activeVisitVideoInfo.video
      )} (caso já possua localizações nesta visita os vídeos das mesmas serão versões curtas)\nLink para edição - https://mixmyvisit.web.ua.pt/redirect/visita/personalizada/${
        activeVisit.tagID
      }?to=editor`,
    };
  } catch (err) {
    const genericError = {
      text:
        'Pedimos desculpa, ocorreu um erro durante a verificação e obtenção da informação da sua visita ativa. Tente mais tarde ou se o problema continuar a persistir contacte-nos via email: mixmv.ua@gmail.com',
    };

    console.log(
      '--VIEWACTIVEVISIT-- !!!!!! Error getting active visit visit =>'
    );

    if (err.response.data) {
      if (err.response.data.message === 'User has no active visit') {
        return {
          text:
            'Neste momento não possuí nenhuma visita ativa o que torna o efeito deste comando nulo. Para começar uma nova visita necessita de utilizar recorrer à aplicação MixMyVisit e criar uma visita personalizada e obter um PIN específico para começar a interagir com o bot e utilizar este comando/funcionalidade.',
        };
      } else {
        return genericError;
      }
    } else if (err.request) {
      return genericError;
    } else {
      return genericError;
    }
  }
};
