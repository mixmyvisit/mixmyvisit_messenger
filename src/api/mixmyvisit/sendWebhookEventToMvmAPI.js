const axios = require('axios');

// Sends a Webhook event to the laravel Mvm API
module.exports = function sendWebhookEventToMvmAPI(
  sender_psid,
  type,
  payload,
  timestamp,
  description
) {
  const request_body = {
    psid: sender_psid,
    type: type,
    payload: payload,
    timestamp: timestamp,
    description: description,
  };

  axios
    .post('https://mixmyvisit.web.ua.pt/api/messenger', request_body, {
      headers: { Authorization: `Bearer ${process.env.API_CLIENT}` },
    })
    .then(() => {
      console.log(
        '--SENDWEBHOOKTOMVMAPI-- $$$$$$ Request sent to Mvm API with success'
      );
    })
    .catch(() => {
      console.log(
        '--SENDWEBHOOKTOMVMAPI-- !!!!!! Error attempting request to Mvm API'
      );
    });
};
