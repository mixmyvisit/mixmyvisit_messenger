const axios = require('axios');

module.exports = async function verifyPinAndRetrieveTagId(receivedPin) {
  try {
    const getTagId = await axios.post(
      'https://mixmyvisit.web.ua.pt/api/messenger-pins/show',
      { pin: receivedPin },
      {
        headers: {
          Authorization: `Bearer ${process.env.API_CLIENT}`,
        },
      }
    );

    const tagId = getTagId.data.tagID;

    await axios.post(
      'https://mixmyvisit.web.ua.pt/api/messenger-pins/delete',
      { pin: receivedPin },
      {
        headers: {
          Authorization: `Bearer ${process.env.API_CLIENT}`,
        },
      }
    );

    return tagId;
  } catch (err) {
    console.log('--VERIFYPIN-- $$$$$$ Verify PIN with error');

    const genericError = {
      text:
        'Pedimos desculpa, ocorreu um erro durante processo de verificar a veracidade do PIN que inseriu no chat. Tente mais tarde ou se o problema continuar a persistir contacte-nos via email: mixmv.ua@gmail.com',
    };

    if (err.response.data) {
      if (err.response.data.message === 'Inserted PIN not found or expired') {
        return {
          text:
            'O PIN que inseriu não é válido ou já está expirado, por favor insira somente o PIN que lhe foi apresentado(a) quando foi redirecionado(a) a partir da aplicação mobile do MixMyVisit e tenha em atenção o tempo limite até o PIN expirar',
        };
      } else {
        return genericError;
      }
    } else if (err.request) {
      return genericError;
    } else {
      return genericError;
    }
  }
};
