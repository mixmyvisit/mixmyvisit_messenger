const axios = require('axios');
const sendResponseViaMessengerAPI = require('../../messenger/sendResponseViaMessengerAPI');

module.exports = async function getUserEmail(sender_psid) {
  try {
    const getEmail = await axios.get(
      `https://mixmyvisit.web.ua.pt/api/webhook?search=psid&value=${sender_psid}`,
      { headers: { Authorization: `Bearer ${process.env.API_CLIENT}` } }
    );

    return getEmail.data.email;
  } catch (err) {
    return sendResponseViaMessengerAPI(sender_psid, {
      text:
        'Pedimos desculpa, ocorreu um erro ao obter o seu email para completar o processo de registo da pulseira, tente novamente mais tarde ao inserir o comando - !criar_visita.\nCaso o problema persista contacte-nos via email - mixmv.ua@gmail.com',
    });
  }
};
