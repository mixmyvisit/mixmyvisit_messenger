const axios = require('axios');
const mvmCache = require('../../../cache/index');
const sendResponseViaMessengerAPI = require('../../messenger/sendResponseViaMessengerAPI');
const registerTag = require('../../altice/registerTag');

module.exports = async function registerUserInMVM(sender_psid) {
  let userInfo;

  try {
    const reqUserInfo = await axios.get(
      `https://graph.facebook.com/${sender_psid}`,
      {
        params: {
          fields: 'first_name,last_name',
          access_token: process.env.PAGE_ACCESS_TOKEN,
        },
      }
    );

    userInfo = reqUserInfo.data;
  } catch (err) {
    console.log('######## failure in graph fb request', err.response);
    mvmCache.set(`writeusername-${sender_psid}`, { writeUsername: true }, 3600);

    if (err.response.data) {
      const errorObj = err.response.data.error;

      if (
        errorObj.error_subcode === 10 ||
        errorObj.error_subcode === 2018218 ||
        errorObj.error_subcode === 2018247
      ) {
        return sendResponseViaMessengerAPI(sender_psid, {
          text:
            'Não conseguimos obter o seu nome via o seu perfil do seu Facebook.\nAssim, pedimos que escreva, manualmente, o nome que pretende utilizar na sua conta do MixMyVisit para efetuarmos o seu registo',
        });
      } else {
        return sendResponseViaMessengerAPI(sender_psid, {
          text:
            'Não conseguimos obter o seu nome via o seu perfil do seu Facebook.\nAssim, pedimos que escreva, manualmente, o nome que pretende utilizar na sua conta do MixMyVisit para efetuarmos o seu registo',
        });
      }
    } else {
      return sendResponseViaMessengerAPI(sender_psid, {
        text:
          'Não conseguimos obter o seu nome via o seu perfil do seu Facebook.\nAssim, pedimos que escreva, manualmente, o nome que pretende utilizar na sua conta do MixMyVisit para efetuarmos o seu registo',
      });
    }
  }

  try {
    const userEmail = mvmCache.take(`email-${sender_psid}`);
    await axios.post(
      'https://mixmyvisit.web.ua.pt/api/webhook',
      {
        email: userEmail.email,
        username: `${userInfo.first_name} ${userInfo.last_name}`,
        psid: sender_psid,
      },
      { headers: { Authorization: `Bearer ${process.env.API_CLIENT}` } }
    );
    
    if (mvmCache.has(`tagid-${sender_psid}`)) {
      const response = await registerTag(sender_psid, null, userEmail);
      return response;
    } else {
      return sendResponseViaMessengerAPI(sender_psid, {
        text:
          'Para ver os comandos disponíveis insira !comandos e caso precise de ajuda escreva !ajuda. Caso esteja com alguma dúvida visite o nosso website: https://mixmyvisit.web.ua.pt/frontpage-info e explore a secção de sobre.',
      });
    }
  } catch (err) {
    console.log('######## failure creating user', err);
    return sendResponseViaMessengerAPI(sender_psid, {
      text:
        'Pedimos desculpa mas ocorreu um erro na criação da sua conta, tente novamente mais tarde ao inserir o comando !registar.\nCaso o problema persista contacte-nos via email.',
    });
  }
};
