const axios = require('axios');
const sendResponseViaMessengerAPI = require('../../messenger/sendResponseViaMessengerAPI');

module.exports = async function checkIfUserExists(
  sender_psid,
  successFunction
) {
  try {
    await axios.get(
      `https://mixmyvisit.web.ua.pt/api/webhook?search=psid&value=${sender_psid}`,
      {
        headers: { Authorization: `Bearer ${process.env.API_CLIENT}` },
      }
    );

    successFunction();
  } catch (err) {
    console.log('######## failure checking if user exists', err.response);
    if (!err.response) {
      sendResponseViaMessengerAPI(sender_psid, {
        text:
          'Pedimos desculpa mas não conseguimos verificar se a conta existe.\nPor favor tente mais tarde ou se o problema persistir contacte-nos via email',
      });
    } else if (
      err.response.status === 404 &&
      err.response.statusText === 'Not Found'
    ) {
      sendResponseViaMessengerAPI(sender_psid, {
        text:
          'Este comando só pode ser realizado por utilizadores que se encontram registados na plataforma MixMyVisit, para se registar insira o comando: "registar"',
      });
    } else {
      sendResponseViaMessengerAPI(sender_psid, {
        text:
          'Pedimos desculpa mas não conseguimos verificar se a conta existe.\nPor favor tente mais tarde ou se o problema persistir contacte-nos via email',
      });
    }
  }
};
