const axios = require('axios');

// Sends response messages via the Facebook Messenger Send API
module.exports = async function sendResponseViaMessengerAPI(
  sender_psid,
  response
) {
  // Construct the message body
  const request_body = {
    recipient: {
      id: sender_psid,
    },
    message: response,
  };

  await axios
    .post(
      `https://graph.facebook.com/v2.6/me/messages?access_token=${process.env.PAGE_ACCESS_TOKEN}`,
      request_body
    )
    .then(() => {
      console.log('message sent!');
    })
    .catch((err) => {
      console.log(err.response);
      console.error(`Unable to send message: ${err.response.statusCode}`);
    });
};
