const axios = require('axios');
const mvmCache = require('../../cache/index');
const requestCreateVisit = require('../mixmyvisit/visit/createVisit');
const sendResponseViaMessengerAPI = require('../messenger/sendResponseViaMessengerAPI');
const getUserEmail = require('../mixmyvisit/user/getUserEmail');

module.exports = async function registerTag(
  sender_psid,
  tag,
  userEmail = null
) {
  let tagID;
  let email = userEmail || null;

  if (mvmCache.has(`tagid-${sender_psid}`)) {
    tagID = mvmCache.take(`tagid-${sender_psid}`);
  }

  const getTag = tag || tagID.tag;

  if (tag || tagID.tag) {
    if (!email) {
      email = await getUserEmail(sender_psid);
    }

    try {
      await axios.get(`${process.env.ALTICE_API}/register`, {
        params: {
          tagExternalId: getTag,
          email: email,
        },
      });

      // notifying that bot will create a visit with tag ID
      return await requestCreateVisit(getTag, sender_psid);
    } catch (reqAlticeErr) {
      return sendResponseViaMessengerAPI(sender_psid, {
        text:
          'Pedimos desculpa mas ocorreu um erro a registar a sua pulseira, tente novamente mais tarde ao inserir o comando - !criar_visita.\nCaso o problema persista contacte-nos via email - mixmv.ua@gmail.com',
      });
    }
  } else {
    // theres no tag id, meaning that the id provided in the m.me link expired
    return sendResponseViaMessengerAPI(sender_psid, {
      text:
        'Pedimos desculpa mas não possui o identificador de uma pulseira ou o identificador da mesma expirou o que impediu o registo do mesmo e a criação da visita.',
    });
  }
};
