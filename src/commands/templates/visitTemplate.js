module.exports = {
  getVisitInfoTemplate: {
    attachment: {
      type: 'template',
      payload: {
        template_type: 'button',
        text:
          'Para aceder ao vídeo final da sua visita necessitará de escrever o comando !concluir_visita. A partir deste ponto nós iremos começar o processo de renderização de todos os vídeos e imagens da sua visita.\nSerá notificado(a) quando vídeo da sua visita será renderizado, podendo fazer download do mesmo se pretender após o processo ter terminado.',
        buttons: [
          {
            type: 'postback',
            title: '!concluir_visita',
            payload: 'confirmation_get_visit',
          },
        ],
      },
    },
  },
  viewVisitInfoTemplate: {
    attachment: {
      type: 'template',
      payload: {
        template_type: 'button',
        text:
          'Pode acompanhar o atual estado do vídeo da sua visita realizando o comando !info_visita, podendo ver uma série de características da sua visita.\nPara uma maior informação e interatividade explore a visita no editor do nosso website usando o comando !editor',
        buttons: [
          {
            type: 'postback',
            title: '!info_visita',
            payload: 'VIEW_VISIT',
          },
        ],
      },
    },
  },
  confirmationGetVisitTemplate: {
    attachment: {
      type: 'template',
      payload: {
        template_type: 'button',
        text:
          'Tem a certeza que quer terminar e renderizar a sua atual visita ativa?\nNão poderá voltar a ativá-la portanto certifique-se mesmo se quer realizar esta ação.',
        buttons: [
          {
            type: 'postback',
            title: 'Não',
            payload: 'GET_VISIT_NO',
          },
          {
            type: 'postback',
            title: 'Sim',
            payload: 'GET_VISIT_YES',
          },
        ],
      },
    },
  },
  decideLocationVideosLength: {
    attachment: {
      type: 'template',
      payload: {
        template_type: 'button',
        text:
            'Antes de começar o processo de renderizar da sua visita necessita de escolhar a duração dos vídeos das localizações. Pode escolher entre versões curtas ou longas, sendo que as versões longas duram mais, em média, aproximademente, 1 a 2 minutos.\nCaso escolha as versões longas dos vídeos a sua visita irá demorar mais tempo no processo de render.\nTerá de escolher dentro de dois minutos, caso contrário terá de inserir novamente o comando !concluir_visita',
        buttons: [
          {
            type: 'postback',
            title: 'Vídeos longos',
            payload: 'FINISH_VISIT_LONG_LOCATION_VIDEOS',
          },
          {
            type: 'postback',
            title: 'Vídeos curtos',
            payload: 'FINISH_VISIT_SHORT_LOCATION_VIDEOS',
          },
        ],
      },
    },
  },
};
