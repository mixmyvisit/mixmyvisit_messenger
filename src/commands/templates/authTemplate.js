module.exports = {
  /*
  |-------------------------------------------------------------------------------
  | Quick Response Template to ask for the email
  |-------------------------------------------------------------------------------
  */

  askEmailQuickResponse: {
    text:
      'Por favor carregue no botão que possui o seu email para continuar com o processo de verificação registo.\nAntes de escolher o email apresentado, por favor verifique que este também é o email principal da sua conta Facebook de forma a não causar erros caso pretenda utilizar o método de autenticação via Facebook no nosso website.\nCaso não surja nenhum botão com um email, por favor escreva-o manualmente',
    quick_replies: [
      {
        content_type: 'user_email',
      },
    ],
  },

  /*
  |-------------------------------------------------------------------------------
  | Template to register a new email
  |-------------------------------------------------------------------------------
  */

  registerEmailTemplate: {
    attachment: {
      type: 'template',
      payload: {
        template_type: 'button',
        text: 'Pretende associar este email com uma nova conta no MixMyVisit?',
        buttons: [
          {
            type: 'postback',
            title: 'Não',
            payload: 'register_email_new_no',
          },
          {
            type: 'postback',
            title: 'Sim',
            payload: 'register_email_new_yes',
          },
        ],
      },
    },
  },

  /*
  |-------------------------------------------------------------------------------
  | Template to ask if the unregistered user can provide his email
  |-------------------------------------------------------------------------------
  */

  userNotRegisteredAskCanProvideEmail: {
    attachment: {
      type: 'template',
      payload: {
        template_type: 'button',
        text:
          'Para relacionarmos a sua conta com o seu identificador do Facebook Messenger necessitamos do seu email, poderia fornecê-lo?',
        buttons: [
          {
            type: 'postback',
            title: 'Não',
            payload: 'provide_email_no',
          },
          {
            type: 'postback',
            title: 'Sim',
            payload: 'provide_email_yes',
          },
        ],
      },
    },
  },

  /*
  |-------------------------------------------------------------------------------
  | Template to ask if the user can provide the email
  |-------------------------------------------------------------------------------
  */

  userFirstTimeAskCanProvideEmail: {
    attachment: {
      type: 'template',
      payload: {
        template_type: 'button',
        text:
          'Para avançarmos com o processo de registo na nossa plataforma necessitamos do seu email, poderia fornecê-lo?',
        buttons: [
          {
            type: 'postback',
            title: 'Não',
            payload: 'provide_email_no',
          },
          {
            type: 'postback',
            title: 'Sim',
            payload: 'provide_email_yes',
          },
        ],
      },
    },
  },
};
