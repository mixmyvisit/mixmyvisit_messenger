module.exports = {
  /*
  |-------------------------------------------------------------------------------
  | Help command Template
  |-------------------------------------------------------------------------------
  */

  helpCommandTemplate: {
    attachment: {
      type: 'template',
      payload: {
        template_type: 'button',
        text: 'Em que tópico é que posso ajudá-lo?',
        buttons: [
          {
            type: 'postback',
            title: 'Começar experiência',
            payload: 'display_experience_list',
          },
          {
            type: 'postback',
            title: 'Comandos',
            payload: 'display_commands_list',
          },
          {
            type: 'postback',
            title: 'Visita',
            payload: 'display_help_visit_list',
          },
        ],
      },
    },
  },

  /*
  |-------------------------------------------------------------------------------
  | Help command --> visit related commands
  |-------------------------------------------------------------------------------
  */

  helpCommandVisitRelatedTemplate: {
    attachment: {
      type: 'template',
      payload: {
        template_type: 'button',
        text: 'Secção de ajuda - visita',
        buttons: [
          {
            type: 'postback',
            title: 'Adição de conteúdos à visita',
            payload: 'display_upload_file_message',
          },
          {
            type: 'postback',
            title: 'Verificar visita',
            payload: 'display_view_visit_command',
          },
          {
            type: 'postback',
            title: 'Obter visita',
            payload: 'display_get_visit_command',
          },
        ],
      },
    },
  },

  /*
  |-------------------------------------------------------------------------------
  | Commands command Template
  |-------------------------------------------------------------------------------
  */

  commandsListsTemplate: {
    attachment: {
      type: 'template',
      payload: {
        template_type: 'button',
        text:
          'Para executar um comando pode escrevê-lo ou carregá-lo na lista apresentada. Os comandos disponíveis atualmente são o seguintes:',
        buttons: [
          {
            type: 'postback',
            title: '!ajuda',
            payload: 'display_help_command',
          },
          {
            type: 'postback',
            title: '!info_visita',
            payload: 'display_view_visit_command',
          },
          {
            type: 'postback',
            title: '!concluir_visita',
            payload: 'display_get_visit_command',
          },
        ],
      },
    },
  },
};
