'use strict';

// Imports dependencies and set up http server
require('dotenv').config();

const axios = require('axios');
const fs = require('fs');
const https = require('https');
const express = require('express');
const bodyParser = require('body-parser');
const validator = require('validator');
// creates express http server
const app = express().use(bodyParser.json());

// Sets server port and logs message on success
const server = https.createServer(
  {
    key: fs.readFileSync(process.env.KEY_PATH),
    cert: fs.readFileSync(process.env.CERT_PATH),
  },
  app
);

server.listen(process.env.PORT, () => {
  console.log(`server starting on port : ${process.env.PORT}`);
});

// Creates the endpoint for our webhook
app.post('/webhook', (req, res) => {
  const body = req.body;
  // Checks this is an event from a page subscription
  if (body.object === 'page') {
    // Iterates over each entry - there may be multiple if batched
    body.entry.forEach(function (entry) {
      // Gets the message. entry.messaging is an array, but
      // will only ever contain one message, so we get index 0
      const webhook_event = entry.messaging[0];
      const sender_psid = webhook_event.sender.psid;

      if (sender_psid !== 100148551609574) {
        // Check if the event is a message or postback and
        // pass the event to the appropriate handler function
        if (webhook_event.message) {
          handleMessage(webhook_event);
        } else if (webhook_event.postback) {
          handlePostback(webhook_event);
          /* } else if (webhook_event.referral) {
          handleMessagingReferrals(webhook_event); */
        } else if (webhook_event.read) {
          console.log('User read the message at %d', webhook_event.timestamp);
        } else {
          console.log(
            'Webhook received unknown webhook_event: ',
            webhook_event
          );
        }
      }
    });

    // Returns a '200 OK' response to all requests
    res.status(200).send('EVENT_RECEIVED');
  } else {
    // Returns a '404 Not Found' if event is not from a page subscription
    res.sendStatus(404);
  }
});

// Adds support for GET requests to our webhook
app.get('/webhook', (req, res) => {
  // Your verify token. Should be a random string.
  const VERIFY_TOKEN = process.env.VERIFY_TOKEN;

  // Parse the query params
  const mode = req.query['hub.mode'];
  const token = req.query['hub.verify_token'];
  const challenge = req.query['hub.challenge'];

  // Checks if a token and mode is in the query string of the request
  if (mode && token) {
    // Checks the mode and token sent is correct
    if (mode === 'subscribe' && token === VERIFY_TOKEN) {
      // Responds with the challenge token from the request
      console.log('WEBHOOK_VERIFIED');
      res.status(200).send(challenge);
    } else {
      // Responds with '403 Forbidden' if verify tokens do not match
      res.sendStatus(403);
    }
  }
});

// get Cache
const mvmCache = require('./cache/index');

// API METHODS
// Specific requests to Messenger
const sendResponseViaMessengerAPI = require('./api/messenger/sendResponseViaMessengerAPI');
// Specific requests to Altice API
const registerTag = require('./api/altice/registerTag');
// Specific requests to Mvm API
const sendWebhookEventToMvmAPI = require('./api/mixmyvisit/sendWebhookEventToMvmAPI');
const requestViewActiveVisitInfo = require('./api/mixmyvisit/visit/viewActiveVisitInfo');
const requestViewActiveVisitState = require('./api/mixmyvisit/visit/viewActiveVisitState');
const requestAskForRender = require('./api/mixmyvisit/visit/askVisitRender');
const requestSetComment = require('./api/mixmyvisit/visit/setCommentInFile');
const registerUserInMVM = require('./api/mixmyvisit/user/registerUser');
const checkIfUserExists = require('./api/mixmyvisit/user/checkIfUserExists');
const checkActiveVisitAndRendering = require('./api/mixmyvisit/visit/checkActiveVisitAndRendering');
const verifyPinAndRetrieveTagId = require('./api/mixmyvisit/user/verifyPinAndRetrieveTagId');

// Command templates
const helpTemplate = require('./commands/templates/helpTemplate');
const visitTemplate = require('./commands/templates/visitTemplate');
const authTemplate = require('./commands/templates/authTemplate');

// Handles messages events
async function handleMessage(received_message) {
  const sender_psid = received_message.sender.id;
  const recipientID = received_message.recipient.id;
  const timeOfMessage = received_message.timestamp;
  const message = received_message.message;
  const eventDescription = `Received message for user ${sender_psid} and page ${recipientID} at ${timeOfMessage} with message: '${JSON.stringify(
    message
  )}'`;

  console.log(
    '####### Received message for user %d and page %d at %d with message:',
    sender_psid,
    recipientID,
    timeOfMessage,
    received_message,
    JSON.stringify(received_message)
  );

  // Check if the message contains text, has attachments or its a quick reply
  if (message.text) {
    if (message.quick_reply) {
      sendWebhookEventToMvmAPI(
        sender_psid,
        'QUICK_REPLY',
        message.quick_reply.payload,
        timeOfMessage,
        eventDescription
      );
      handleQuickReplies(sender_psid, message.quick_reply.payload);
    } else {
      sendWebhookEventToMvmAPI(
        sender_psid,
        'TEXT_MESSAGE',
        message.text,
        timeOfMessage
      );
      handleTextMessages(sender_psid, message.text);
    }
  } else if (message.attachments) {
    // if there are any attachments verify if the sender_psid of the user is registered in the DB and if it has a active visit
    await checkIfUserExists(sender_psid, async () => {
      await checkActiveVisitAndRendering(sender_psid, async (paramTagId) => {
        const attachment_url = message.attachments[0].payload.url;

        sendWebhookEventToMvmAPI(
          sender_psid,
          'FILE_UPLOAD',
          attachment_url,
          timeOfMessage,
          eventDescription
        );

        // if user sent more than one file
        if (message.attachments.length > 1) {
          sendResponseViaMessengerAPI(sender_psid, {
            text:
              'Inseriu mais do que um ficheiro na sua mensagem.\nDe forma a que o processo de adicionar uma imagem ou vídeo à sua visita funcione certifique-se de colocar somente um ficheiro',
          });
        } else {
          // Find if there are files with the incorrect types (audio and file types are not supported for this action)
          const checkIncorrectTypeFiles = message.attachments.find(
            (item) =>
              item.type === 'audio' ||
              item.type === 'file' ||
              item.type === 'fallback'
          );
          const checkFallbackType = message.attachments.find(
            (item) => item.type === 'fallback'
          );

          if (!checkFallbackType || !checkIncorrectTypeFiles) {
            sendWebhookEventToMvmAPI(
              sender_psid,
              'FILE_UPLOAD',
              attachment_url,
              timeOfMessage,
              eventDescription
            );

            handleAttachments(sender_psid, attachment_url);
          } else {
            // send specific message if user sent video/audio/other type files
            sendResponseViaMessengerAPI(sender_psid, {
              text: `Pedimos desculpa mas ocorreu um erro ao fazer upload do seu ficheiro.\nEste erro pode ocorrer se realizou o upload de um ficheiro de vídeo ou de áudio ou de outro tipo que não seja uma imagem. Caso tenha feito upload de um vídeo que queira adicionar à sua visita faço-o a partir do comando !adicionar_conteudo ou então via editor ao recorrer ao comando "editor"`,
            });
          }
        }
      });
    });
  }
}

// Handles messages the user sends; checks for commands; calls sendCommandMessage function to keep this function cleaner
async function handleTextMessages(sender_psid, message) {
  // firstly we need to verify if there are certain values in the cache (stored to handle specific situations) before deciding which request/command to use/show

  if (mvmCache.has(`file-${sender_psid}`)) {
    // checking if there is a file in the cache with user psid to set a specific comment to a certain image the user uploaded
    const fileInfo = mvmCache.take(`file-${sender_psid}`);
    const commentResponse = await requestSetComment(
      sender_psid,
      fileInfo.file,
      message
    );
    sendResponseViaMessengerAPI(sender_psid, commentResponse);
    mvmCache.has(`file-${sender_psid}`) && mvmCache.del(`file-${sender_psid}`);
  } else if (mvmCache.has(`writeusername-${sender_psid}`)) {
    // checking if there is a instance of writeusername in the cache with user psid, meaning the user will need to write their username before a certain time limit. This condition only exists because there is a chance that we cant find the name from the Facebook API of the user so we need to do some logic in order for the user to write it manually
    const checkUsername = validator.isLength(message, { min: 4, max: 16 });

    if (checkUsername) {
      mvmCache.del(`writeusername-${sender_psid}`);

      try {
        const userEmail = mvmCache.take(`email-${sender_psid}`);
        await axios.post(
          'https://mixmyvisit.web.ua.pt/api/webhook',
          {
            email: userEmail.email,
            username: `${message}`,
            psid: sender_psid,
          },
          {
            headers: { Authorization: `Bearer ${process.env.API_CLIENT}` },
          }
        );

        await sendResponseViaMessengerAPI(sender_psid, {
          text:
            'Registo realizado com sucesso! Irá receber posteriormente um email com um link para confirmar o seu email e para definir a sua palavra-passe se desejar.',
        });

        if (mvmCache.has(`tagid-${sender_psid}`)) {
          await checkActiveVisitAndRendering(sender_psid, null, async () => {
            await registerTag(sender_psid, null, userEmail);
          });
        } else {
          sendResponseViaMessengerAPI(sender_psid, {
            text:
              'Para ver os comandos disponíveis insira "comandos" e caso precise de ajuda escreva "ajuda". Caso esteja com alguma dúvida visite o nosso website: https://mixmyvisit.web.ua.pt/frontpage-info e explore a secção de sobre.',
          });
        }
      } catch (err) {
        console.log('######## failure creating user', err);
        sendResponseViaMessengerAPI(sender_psid, {
          text:
            'Pedimos desculpa mas ocorreu um erro na criação da sua conta, tente novamente mais tarde ao inserir o comando "registar".\nCaso o problema persista contacte-nos via email.',
        });
      }
    } else if (/^[cC]ancelar [rR]egisto$/g.test(message)) {
      // command for the user to cancel their registation process if they for some reason give up of writing their name, just for precaution, it maybe never be used to be honest
      mvmCache.del(`writeusername-${sender_psid}`);
      sendResponseViaMessengerAPI(sender_psid, {
        text:
          'O processo de registo foi cancelado. Pode sempre retomá-lo quando quiser utilizando o comando "registar"',
      });
    } else {
      sendResponseViaMessengerAPI(sender_psid, {
        text:
          'O username que inseriu não segue os critérios estipulados de pelo menos 4 caracteres e não mais de 16 caracteres. Por favor escreva um username que siga os critérios referidos anteriormente.\nCaso queira cancelar este processo insira o comando "cancelar visita"',
      });
    }
  } else if (mvmCache.has(`writeemail-${sender_psid}`)) {
    // checking if there is a instance of writeusername in the cache with user psid, meaning the user will need to write their email before a certain time limit
    // we get the email the user wrote and do a validation and check if its already used in the DB, 100% of the times is not in the DB but for precaution theres actions if we find the email in the DB

    const checkEmail = validator.isEmail(message);

    if (checkEmail) {
      mvmCache.del(`writeemail-${sender_psid}`);

      const request_body = {
        email: message,
        user_psid: sender_psid,
      };

      axios
        .put('https://mixmyvisit.web.ua.pt/api/webhook', request_body, {
          headers: { Authorization: `Bearer ${process.env.API_CLIENT}` },
        })
        .then(async () => {
          // the following code inside the try happens when there is a user that is registered already in the website and then 'tries' to create an account via the bot, which wont happen by normal means (users following the supposed flow wont get in here)
          sendResponseViaMessengerAPI(sender_psid, {
            text:
              'O email que forneceu já se encontra registado e foi agora relacionado com o seu identificador do Facebook Messenger.\nPor favor aguarde um momento, sem inserir comandos, enquanto registamos a sua pulseira para começar a sua visita.\n',
          });

          await checkActiveVisitAndRendering(sender_psid, null, async () => {
            await registerTag(sender_psid, message);
          });
        })
        .catch((_) => {
          mvmCache.set(`email-${sender_psid}`, { email: message }, 1000);
          sendResponseViaMessengerAPI(sender_psid, {
            attachment: {
              type: 'template',
              payload: {
                template_type: 'button',
                text: `O email que forneceu parece que ainda não foi registado. O email ${message} está correto?`,
                buttons: [
                  {
                    type: 'postback',
                    title: 'Sim',
                    payload: 'link_email_was_correct',
                  },
                  {
                    type: 'postback',
                    title: 'Não',
                    payload: 'link_email_was_not_correct',
                  },
                ],
              },
            },
          });
        });
    } else if (/^[cC]ancelar [eE]mail$/g.test(message)) {
      mvmCache.del(`writeemail-${sender_psid}`);
      const responseStopWritingEmail =
        authTemplate.userFirstTimeAskCanProvideEmail;
      sendResponseViaMessengerAPI(sender_psid, responseStopWritingEmail);
    } else {
      sendResponseViaMessengerAPI(sender_psid, {
        text:
          'O email que inseriu não possui o formato standard de um email. Por favor verifique se inseriu o seu email corretamente.\nCaso pretenda sair do modo de escrever o seu email use o comando "cancelar_esc_email"',
      });
    }
  } else {
    // default switch statement to compare the text message the user sent to the bot in order to see if its needed to perform a certain action/command
    switch (true) {
      case /^PIN\d{4}$/g.test(message):

          const getPin = message.replace(/\D/g, '');
          const tagId = await verifyPinAndRetrieveTagId(getPin, sender_psid);

          if (tagId) {
            mvmCache.set(`tagid-${sender_psid}`, { tag: tagId }, 3600);

            try {
              await sendResponseViaMessengerAPI(sender_psid, {
                text:
                  'Bem-vindo(a) ao bot da aplicação MixMyVisit. Verificamos que possuí um PIN válido relacionado com uma pulseira.\nAguarde uns segundos enquanto começamos o processo de registo.',
              });

              // search in DB to see if there's a user with the psid
              const response = await axios.get(
                `https://mixmyvisit.web.ua.pt/api/webhook?search=psid&value=${sender_psid}`,
                {
                  headers: {
                    Authorization: `Bearer ${process.env.API_CLIENT}`,
                  },
                }
              );

              switch (response.data.data) {
                // linked means user is registered and already interacted with the bot, so the the DB knows its psid
                case 'linked':
                  await sendResponseViaMessengerAPI(sender_psid, {
                    text:
                      'Verificamos que já se encontra registado na nossa aplicação e que inseriu um PIN associado à pulseira que está a utilizar na sua visita.\nAguarde um momento enquanto procedemos para o registo da pulseira e a criação da sua visita',
                  });

                  return checkActiveVisitAndRendering(
                    sender_psid,
                    null,
                    async () => {
                      const regTag = await registerTag(sender_psid);

                      return regTag;
                    }
                  );

                case 'not_linked':
                  // user already has an account but need to link it to save his psid in our DB
                  await sendResponseViaMessengerAPI(sender_psid, {
                    text:
                      'Verificamos que já se encontra registado(a) na nossa aplicação mas a sua conta ainda não articulada com o Facebook Messenger.\nPor favor siga as seguintes intruções para realizar este processo',
                  });

                  return sendResponseViaMessengerAPI(
                    sender_psid,
                    authTemplate.userNotRegisteredAskCanProvideEmail
                  );
                default:
                  return null;
              }
            } catch (err) {
              // user psid is not registered in our DB
              await sendResponseViaMessengerAPI(sender_psid, {
                text:
                  'Não encontramos nenhuma conta na nossa base de dados com o seu identificador do Facebook Messenger. Por favor, siga as seguintes instruções para realizarmos o processo de registo na plataforma MixMyVisit',
              });
              return sendResponseViaMessengerAPI(
                sender_psid,
                authTemplate.userFirstTimeAskCanProvideEmail
              );
            }
          } else {
            await sendResponseViaMessengerAPI(sender_psid, {
              text:
                'O PIN que introduziu não é válido, pode já ter expirado ou ser inválido.\nPara obter um PIN por favor dirija-se ao dispositivo dedicado à nossa aplicação no local da sua visita e interaja com a sua pulseira NFC na aplicação.\nSe necessitar de ajuda nesta e noutras questões utilize o comando "ajuda"',
            });
          }

        break;
      case /^[aA]juda$/g.test(message):
        sendResponseViaMessengerAPI(
          sender_psid,
          helpTemplate.helpCommandTemplate
        );
        break;
      case /^[cC]omandos?$/g.test(message):
        sendResponseViaMessengerAPI(
          sender_psid,
          helpTemplate.commandsListsTemplate
        );
        break;
      case /^[cC]omandos [vV]isita?$/g.test(message):
        sendResponseViaMessengerAPI(
          sender_psid,
          helpTemplate.helpCommandVisitRelatedTemplate
        );
        break;
      case /^[rR]egistar?$/g.test(message):
        let template;

        try {
          await sendResponseViaMessengerAPI(sender_psid, {
            text:
              'Espere só um segundo enquanto pesquisamos se já se encontra registo na nossa aplicação ao realizar um processo de verificação de registo.',
          });

          // search in DB to see if there's a user with the psid that the chatter has
          const response = await axios.get(
            `https://mixmyvisit.web.ua.pt/api/webhook?search=psid&value=${sender_psid}`,
            { headers: { Authorization: `Bearer ${process.env.API_CLIENT}` } }
          );

          switch (response.data.data) {
            case 'not_linked':
              // user already has an account but need to link it to save his psid in our DB
              await sendResponseViaMessengerAPI(sender_psid, {
                text:
                  'Verificamos que já se encontra registado na nossa aplicação mas a sua conta ainda não articulada com o Facebook Messenger.\nPor favor siga as seguintes intruções para realizar este processo.',
              });
              template = authTemplate.userNotRegisteredAskCanProvideEmail;
              break;
            default:
              template = null;
          }
        } catch (err) {
          // user is not registered in our DB
          await sendResponseViaMessengerAPI(sender_psid, {
            text:
              'Não encontramos nenhuma conta na nossa base de dados com o seu identificador do Facebook Messenger. Por favor siga as seguintes instruções para realizarmos o seu registo.',
          });

          template = authTemplate.userFirstTimeAskCanProvideEmail;
        }

        if (template) {
          sendResponseViaMessengerAPI(sender_psid, template);
        } else {
          sendResponseViaMessengerAPI(sender_psid, {
            text:
              'Verificamos que já se encontra registado na nossa aplicação e a sua conta já foi articulada com o identificador do Facebook Messenger, tornando o efeito deste comando nulo.\nSe necessitar de ajuda escreva o comando "ajuda"',
          });
        }
        break;
      case /^[cC]riar [vV]isita$/g.test(message):
        // this command only exists if the
        const hasTagId = mvmCache.has(`tagid-${sender_psid}`);

        if (hasTagId) {
          await checkIfUserExists(sender_psid, async () => {
            await checkActiveVisitAndRendering(sender_psid, null, async () => {
              await registerTag(sender_psid);
            });
          });
        } else {
          sendResponseViaMessengerAPI(sender_psid, {
            text:
              'Só poderá criar uma visita se já tiver usado um PIN válido neste chat que ainda esteja válido. Para obter um PIN e começar a sua visita necessitará de criar uma visita personalizada na aplicação mobile MixMyVisit',
          });
        }
        break;
      case /^[cC]oncluir [vV]isita$/g.test(message):
        await checkIfUserExists(sender_psid, async () => {
          await checkActiveVisitAndRendering(sender_psid, async () => {
            sendResponseViaMessengerAPI(
              sender_psid,
              visitTemplate.confirmationGetVisitTemplate
            );
          });
        });
        break;
      case /^[iI]nfo [vV]isita$/g.test(message):
        await checkIfUserExists(sender_psid, async () => {
          const responseViewVisit = await requestViewActiveVisitInfo(
            sender_psid
          );
          sendResponseViaMessengerAPI(sender_psid, responseViewVisit);
        });
        break;
      case /^[iI]nfo [eE]stado [vV]isita$/g.test(message):
        await checkIfUserExists(sender_psid, async () => {
          const responseViewVisitState = await requestViewActiveVisitState(
            sender_psid
          );
          sendResponseViaMessengerAPI(sender_psid, responseViewVisitState);
        });
        break;
      case /^[aA]dicionar [cC]onteudo/g.test(message):
        await checkIfUserExists(sender_psid, async () => {
          await checkActiveVisitAndRendering(
            sender_psid,
            async (paramTagId) => {
              sendResponseViaMessengerAPI(sender_psid, {
                text: `Pode adicionar vídeos ou imagens à sua visita através de formas distintas. Caso pretenda adicionar uma imagem, pode fazê-lo ao adicioná-la no chat do bot, arrastando-a para a conversa ou fazendo upload manualmente da mesma via botão no canto inferior esquerdo. Esta imagem será ordenada na sua visita pela data em que foi embutida no chat.\nTambém pode adicionar vídeos ou imagens a partir do seguinte link no nosso website: https://mixmyvisit.web.ua.pt/redirect/messenger/upload-conteudo/${paramTagId}. Este conteúdo será ordenado pela data em que foi criado por si.\nAinda tem a possibilidade de enviar via editor no nosso website ao utilizar o comando "editor", sendo o conteúdo ordenado pela data em que foi adicionado ao editor.\nAconselhamos para estas duas formas que use um browser dedicado (Google Chrome, Mozilla etc.) e não o browser embutido no Facebook Messenger.\nÉ recomendado que a sua imagem ou vídeo tenha um aspect ratio de 16:9 (1920x1080 - orientação horizontal) e seja um ficheiro do tipo JPEG, PNG (imagem) e MP4 (vídeo).`,
              });
            });
        });
        break;
      case /^[eE]ditor$/g.test(message):
        await checkIfUserExists(sender_psid, async () => {
          await checkActiveVisitAndRendering(
            sender_psid,
            async (paramTagId) => {
              sendResponseViaMessengerAPI(sender_psid, {
                text: `Pode editar a sua visita a partir do seguinte link: https://mixmyvisit.web.ua.pt/redirect/visita/personalizada/${paramTagId}?to=editor`,
              });
            }
          );
        });
        break;
      default:
        checkStringRegex(message);
        break;
    }
  }

  function checkStringRegex(sentMessage) {
        sendResponseViaMessengerAPI(sender_psid, {
          text:
            'Desculpe, não consegui reconhecer o comando que inseriu. Para visualizar uma lista de todos os comandos possíveis escreva o comando "comandos"./n Se necessitar de ajuda use o comando "ajuda"',
        });
    }
}

// Handles messaging_postbacks events
async function handlePostback(received_postback) {
  const sender_psid = received_postback.sender.id;
  const recipientID = received_postback.recipient.id;
  const timeOfPostback = received_postback.timestamp;

  // Get the payload for the postback
  const payload = received_postback.postback.payload;

  // description to send to the API
  const eventDescription = `Received postback for user ${sender_psid} and page ${recipientID} with payload '${payload}' at ${timeOfPostback}`;

  sendWebhookEventToMvmAPI(
    sender_psid,
    'POSTBACK',
    payload,
    timeOfPostback,
    eventDescription
  );

  let response;

  // Set the response based on the postback payload
  switch (payload) {
    case 'WHAT_IS_MVM':
      response = {
        text:
          'A aplicação MixMyVisit permite a criação de vídeos automáticos das visitas efetuadas a espaços culturais (ex. museus, parques, exposições, entre outros) com base nos percursos e locais visitados pelos utilizadores',
      };
      break;
    case 'MVM_EXPERIENCE':
      response = {
        text:
          'Para criar uma visita e usufruir das funcionalidades da plataforma MixMyVisit necessitará de obter um PIN de quatro digitos a partir da nossa aplicação mobile que estará presente num local específico cultural/turístico que faço uso da aplicação MixMyVisit para criar visitas dinâmicas do mesmo. Depois de obter o PIN só necessita de escrevê-lo no chat - "PIN****" - sem os asteríscos os números do PIN e o processo de registo e criação da visita começará automaticamente',
      };
      break;
    // user agreed with providing his email
    case 'provide_email_yes':
      mvmCache.set(`writeemail-${sender_psid}`, { writeEmail: true }, 1000);
      response = authTemplate.askEmailQuickResponse;
      break;
    case 'provide_email_no':
      response = {
        text:
          'Cancelou o processo de verificação de registo, se quiser novamente começar este processo escreva o comando "registar"',
      };
      break;
    // API couldn't link the email to a psid --> user answered it WAS correct
    case 'link_email_was_correct':
      mvmCache.has(`writeemail-${sender_psid}`) &&
        mvmCache.del(`writeemail-${sender_psid}`);
      response = authTemplate.registerEmailTemplate;
      break;
    // API couldn't link the email to a psid --> user answered it WAS NOT correct
    case 'link_email_was_not_correct':
      mvmCache.set(`writeemail-${sender_psid}`, { writeEmail: true }, 1000);
      response = authTemplate.askEmailQuickResponse;
      break;
    case 'register_email_new_yes':
      mvmCache.has(`writeemail-${sender_psid}`) &&
        mvmCache.del(`writeemail-${sender_psid}`);
      response = await registerUserInMVM(sender_psid);
      break;
    case 'register_email_new_no':
      response = {
        text: 'Por favor escreva o seu email para o podermos registar',
      };
      break;
    // user uploaded a file
    case 'upload_file':
      response = {
        text:
          'Obrigado! Quando o upload do seu ficheiro terminar será notificado(a)',
      };
      break;
    case 'display_help_command':
      response = helpTemplate.helpCommandTemplate;
      break;
    case 'display_commands_list':
      response = helpTemplate.commandsListsTemplate;
      break;
    case 'display_help_visit_list':
      response = helpTemplate.helpCommandVisitRelatedTemplate;
      break;
    case 'display_upload_file_message':
      response = {
        text:
          'Pode adicionar uma imagem ou vídeo à sua escolha na sua visita ativa ao utilizar o comando - "adicionar conteudo" - sendo que serão explicados os procedimentos necessários ao utilizar o comando.\nÉ recomendado que a sua imagem ou vídeo tenha um aspect ratio de 16:9 (1920x1080) e seja um ficheiro do tipo JPEG, PNG (imagem) e MP4 (vídeo)',
      };
      break;
    case 'display_view_visit_command':
      response = visitTemplate.viewVisitInfoTemplate;
      break;
    case 'display_get_visit_command':
      response = visitTemplate.getVisitInfoTemplate;
      break;
    case 'confirmation_get_visit':
      response = visitTemplate.confirmationGetVisitTemplate;
      break;
    case 'confirmation_finish_visit':
      response = visitTemplate.confirmationFinishVisitTemplate;
      break;
    case 'GET_VISIT_YES':
      mvmCache.set(`end-visit-${sender_psid}`, { isChoosing: 'yes' }, 1000);

      response = visitTemplate.decideLocationVideosLength; // await requestAskForRender(sender_psid);
      break;
    case 'GET_VISIT_NO':
      response = {
        text:
          'Pode terminar e fazer o download da sua visita a qualquer outra altura, só necessita de inserir novamente o comando "concluir visita"',
      };
      break;
    case 'FINISH_VISIT_YES':
      mvmCache.set(`end-visit-${sender_psid}`, { isChoosing: 'yes' }, 1000);

      response = visitTemplate.decideLocationVideosLength; // await requestAskForRender(sender_psid);
      break;
    case 'FINISH_VISIT_NO':
      response = {
        text:
          'Pode terminar a sua visita a qualquer outra altura, só necessita de inserir o comando "concluir visita"',
      };
      break;
    case 'FINISH_VISIT_LONG_LOCATION_VIDEOS':
      response = mvmCache.has(`end-visit-${sender_psid}`)
        ? await requestAskForRender(sender_psid, 'long')
        : {
            text:
              'Este pedido de renderização expirou. Insira o comando "concluir visita" novamente para começar o processo',
          };
      mvmCache.has(`end-visit-${sender_psid}`) &&
        mvmCache.del(`end-visit-${sender_psid}`);

      break;
    case 'FINISH_VISIT_SHORT_LOCATION_VIDEOS':
      response = mvmCache.has(`end-visit-${sender_psid}`)
        ? await requestAskForRender(sender_psid, 'small')
        : {
            text:
              'Este pedido de renderização expirou. Insira o comando "concluir visita" novamente para começar o processo',
          };
      mvmCache.has(`end-visit-${sender_psid}`) &&
        mvmCache.del(`end-visit-${sender_psid}`);
      break;
    case 'VIEW_VISIT':
      response = await requestViewActiveVisitInfo(sender_psid);
      break;
    case 'UPLOAD_FILE_NO_COMMENT':
      mvmCache.has(`file-${sender_psid}`) && mvmCache.del(`file-${sender_psid}`);
      response = {
        text:
          'Caso pretenda adicionar um comentário mais tarde à sua imagem pode fazê-lo no editor da visita no nosso website recorrendo ao comando - "editor" - e se quiser adicionar outro conteúdo necessitará de repetir a ação que acabou de realizar ou utilizar o comando "adicionar_conteudo"',
      };
      break;
    case 'UPLOAD_FILE_COMMENT':
      response = {
        text:
          'Escreva o comentário que pretende adicionar à sua imagem que acabou de adicionar à sua visita, sabendo que o comentário não pode possuir qualquer alusão a um comando do bot nem mais de 100 caracteres.\nTem um limite de 15 minutos para escrever o comentário',
      };
      break;
    /*
    This CASE was responsible for the response of the GET_STARTED payload which would be activated when the user entered the bot via an m.me link with a value in the ref param in the URL
    case 'GET_STARTED':
      if (received_postback.postback.referral.ref !== undefined) {
        mvmCache.set(
            `tagid-${sender_psid}`,
            { tag: received_postback.postback.referral.ref },
            3600
        );
      }

      response = await handleGetStarted(sender_psid);
      break;
      */
    default:
      break;
  }

  // Send the message to acknowledge the postback
  sendResponseViaMessengerAPI(sender_psid, response);
}

// Handles attachments and sends them to the BD
async function handleAttachments(sender_psid, attachment) {
  await sendResponseViaMessengerAPI(sender_psid, {
    text:
      'Inseriu uma imagem que será adicionada à sua visita. Será notificado(a) quando o upload da imagem que submeteu terminou',
  });

  const request_body = {
    file: attachment,
    user_psid: sender_psid,
  };

  try {
    const fileInfo = await axios.post(
      'https://mixmyvisit.web.ua.pt/api/webhook/upload',
      request_body,
      {
        headers: { Authorization: `Bearer ${process.env.API_CLIENT}` },
      }
    );

    mvmCache.set(
      `file-${sender_psid}`,
      { file: fileInfo.data.url },
      1000
    );

    sendResponseViaMessengerAPI(sender_psid, {
      attachment: {
        type: 'template',
        payload: {
          template_type: 'button',
          text:
            'O upload da sua imagem foi concluído com sucesso.\nPretende adicionar um comentário à sua imagem?',
          buttons: [
            {
              type: 'postback',
              title: 'Não',
              payload: 'UPLOAD_FILE_NO_COMMENT',
            },
            {
              type: 'postback',
              title: 'Sim',
              payload: 'UPLOAD_FILE_COMMENT',
            },
          ],
        },
      },
    });
  } catch (err) {
    console.log('--UPLOAD-- !!!!!! Error uploading file =>', err);

    const genericError = {
      text:
        'Pedimos desculpa mas ocorreu um erro durante o upload do seu ficheiro. Tente mais tarde ou se o problema continuar a persistir tente utilizar uma imagem diferente para verificar se o problema reside na imagem que está a fazer upload. Caso contrário, se o problema continuar a ocorrer, contacte-nos via email: mixmv.ua@gmail.com',
    };

    if (err.response.data) {
      if (err.response.data.message === 'User has no active visit') {
        sendResponseViaMessengerAPI(sender_psid, {
          text:
            'Neste momento não possuí nenhuma visita ativa o que torna o efeito deste comando nulo.',
        });
      } else {
        sendResponseViaMessengerAPI(sender_psid, genericError);
      }
    } else if (err.request) {
      sendResponseViaMessengerAPI(sender_psid, genericError);
    } else {
      sendResponseViaMessengerAPI(sender_psid, genericError);
    }
  }
}

// Handles quick replies, specific quick_reply of email confirmation
async function handleQuickReplies(sender_psid, reply_payload) {
  const request_body = {
    email: reply_payload,
    user_psid: sender_psid,
  };

  mvmCache.set(`email-${sender_psid}`, { email: reply_payload }, 1000);

  try {
    await axios.put('https://mixmyvisit.web.ua.pt/api/webhook', request_body, {
      headers: { Authorization: `Bearer ${process.env.API_CLIENT}` },
    });

    // the following code inside the try happens when there is a user that is registered already in the website and then 'tries' to create an account via the bot, which wont happen by normal means (users following the supposed flow wont get in here)
    sendResponseViaMessengerAPI(sender_psid, {
      text:
        'O email que forneceu já se encontra registado e foi agora relacionado com o seu identificador do Facebook Messenger.',
    });

    if (mvmCache.has(`tagid-${sender_psid}`)) {
      sendResponseViaMessengerAPI(sender_psid, {
        text:
          'Verificamos que possuí um identifcador de uma pulseira. Espere uns segundos enquanto realizamos o registo da mesma e a criação de uma visita também associada à pulseira.',
      });

      await checkActiveVisitAndRendering(sender_psid, null, async () =>
        registerTag(sender_psid, null, mvmCache.take(`email-${sender_psid}`))
      );
    } else {
      sendResponseViaMessengerAPI(sender_psid, {
        text:
          '\nPara ver os comandos disponíveis para interagir com o bot insira "comandos" no chat e caso necessite de ajuda escreva "ajuda"',
      });
    }
  } catch (err) {
    sendResponseViaMessengerAPI(sender_psid, {
      attachment: {
        type: 'template',
        payload: {
          template_type: 'button',
          text: `O email que forneceu parece que ainda não foi registado. O email ${reply_payload} está correto?`,
          buttons: [
            {
              type: 'postback',
              title: 'Não',
              payload: 'link_email_was_not_correct',
            },
            {
              type: 'postback',
              title: 'Sim',
              payload: 'link_email_was_correct',
            },
          ],
        },
      },
    });
  }
}